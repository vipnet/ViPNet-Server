#include "server_init.h"

static void linux_lwip_init(void); /* Initializes the stack, sets the callback to tcpip_init_done */
static void tcpip_init_done(void *arg); /* After stack is initialized, set up the gateway ip. Can be overriden in 
										 * main thread from the application.*/
static struct netif netif; /* manual host IP configuration */

static void tcpip_init_done(void *arg)
{
	ip_addr_t ipaddr, netmask, gw;
	sys_sem_t *sem;
	sem = (sys_sem_t *)arg;

	/* default IP for the main_thread application. vpn_server will have to be in same network */
	IP4_ADDR(&gw,       10, 0, 2, 1);
	IP4_ADDR(&netmask,  255, 255, 255, 0);
	IP4_ADDR(&ipaddr,   0, 0, 0, 0);

	/* Create the interface using the IP that was put in main */
	netif_set_default(netif_add(&netif, &ipaddr, &netmask, &gw, NULL, tapif_init, tcpip_input));
	netif_set_up(&netif);

	sys_sem_signal(sem);
}

static void linux_lwip_init(void) {
	sys_sem_t sem;
	char gw_str[16] = {0};
	struct in_addr inaddr;
	err_t err;

	err = sys_sem_new(&sem, 0);
	LWIP_ASSERT("Failed to create semaphore", err == ERR_OK);

	netif_init();
	tcpip_init(tcpip_init_done, &sem);
	sys_sem_wait(&sem);

	inaddr.s_addr = netif_default->gw.addr;
	strncpy(gw_str,inet_ntoa(inaddr),sizeof(gw_str));
	printf("[LWIP] TCP/IP initialized. Gateway: %s \n", gw_str);

	/* This here starts the VPN server on Linux. It also overwrites the IP set above. */
	app_main(NULL);
}

int main(int argc, char **argv) {
	LWIP_UNUSED_ARG(argc);
	LWIP_UNUSED_ARG(argv);

	printf("[LWIP] Starting...\n");
	linux_lwip_init();
	pause();

	return 0;
}







