CCDEP=gcc
CC=gcc

CFLAGS= -std=gnu99 -g -Wall -DLWIP_IPV4 -DLWIP_ETHERNET=1 -DLWIP_PROVIDE_ERRNO -pedantic \
	-Wparentheses -Wsequence-point -Wswitch-default -Wno-variadic-macros \
	-Wextra -Wundef -Wshadow -Wpointer-arith -Wbad-function-cast \
	-Wc++-compat -Wwrite-strings -Wold-style-definition \
	-Wmissing-prototypes -Wredundant-decls -Wnested-externs \
	-DVPN_DEBUG=1 -DLINUX_VPN

LDFLAGS=-lpthread -lutil
ARFLAGS=rs
LWIPARCH=$(CONTRIBDIR)/ports/unix/port
EXE_NAME=vpn_server

CFLAGS:=$(CFLAGS) -I../include -I. \
	-I$(LWIPDIR)/include -I$(LWIPARCH)/include -I$(LWIPDIR)/include/ipv4 \
	-I$(LWIPDIR) 

# COREFILES, CORE4FILES: The minimum set of files needed for lwIP.
COREFILES=$(wildcard $(LWIPDIR)/core/*.c) $(wildcard $(LWIPDIR)/netif/*.c)
CORE4FILES=$(wildcard $(LWIPDIR)/core/ipv4/*.c)

# SNMPFILES: Extra SNMPv1 agent
#SNMPFILES=$(LWIPDIR)/core/snmp/mib2.c $(LWIPDIR)/core/snmp/mib_structs.c \
#	$(LWIPDIR)/core/snmp/msg_in.c $(LWIPDIR)/core/snmp/msg_out.c

# APIFILES: The files which implement the sequential and socket APIs.
APIFILES=$(LWIPDIR)/api/api_lib.c $(LWIPDIR)/api/api_msg.c $(LWIPDIR)/api/tcpip.c \
	$(LWIPDIR)/api/err.c $(LWIPDIR)/api/sockets.c $(LWIPDIR)/api/netbuf.c $(LWIPDIR)/api/netdb.c

# NETIFFILES: Files implementing various generic network interface functions.
#NETIFFILES=$(LWIPDIR)/netif/etharp.c $(LWIPDIR)/netif/slipif.c

# NETIFFILES: Add PPP netif
NETIFFILES+=$(LWIPDIR)/netif/ppp/auth.c \
	$(LWIPDIR)/netif/ppp/fsm.c \
	$(LWIPDIR)/netif/ppp/ipcp.c $(LWIPDIR)/netif/ppp/lcp.c \
	$(LWIPDIR)/netif/ppp/magic.c \
	$(LWIPDIR)/netif/ppp/ppp.c \
	$(LWIPDIR)/netif/ppp/vj.c

# ARCHFILES: Architecture specific files.
ARCHFILES=$(wildcard $(LWIPARCH)/*.c  $(LWIPARCH)/netif/*.c)

# APPFILES: Applications.
APPFILES=$(wildcard ../*.c) lwip_init.c

# LWIPFILES: All the above.
LWIPFILES=$(COREFILES) $(CORE4FILES) $(SNMPFILES) $(APIFILES) $(NETIFFILES) $(ARCHFILES)
LWIPFILESW=$(wildcard $(LWIPFILES))
LWIPOBJS=$(notdir $(LWIPFILESW:.c=.o))

LWIPLIB=liblwip4.a
APPLIB=liblwipapps.a
APPOBJS=$(notdir $(APPFILES:.c=.o))

%.o:
	$(CC) $(CFLAGS) -c $(<:.o=.c)

all ipv4 compile: make_server
.PHONY: all

clean:
	rm -f *.o $(APPLIB) $(LWIPLIB) $(EXE_NAME) *.s .depend* *.core core

depend dep: .depend

include .depend

$(APPLIB): $(APPOBJS)
	$(AR) $(ARFLAGS) $(APPLIB) $?

$(LWIPLIB): $(LWIPOBJS)
	$(AR) $(ARFLAGS) $(LWIPLIB) $?

.depend: $(LWIPFILES) $(APPFILES)
	$(CCDEP) $(CFLAGS) -MM $^ > .depend || rm -f .depend

make_server: .depend $(LWIPLIB) $(APPLIB)
	$(CC) $(CFLAGS) -o $(EXE_NAME) $(APPLIB) $(LWIPLIB) $(LDFLAGS)

