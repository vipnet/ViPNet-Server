#ifndef TCP_SERVER_H
#define TCP_SERVER_H

#include <server_init.h>
#include <utils.h>
#include <lwip/tcp.h>

struct TCP_client {
	int index;
	unsigned char mac[MAC_SIZE];
	ip_addr_t ip;
	struct tcp_pcb *pcb;
};

/* Constants */
#define BUFSIZE 1514
#define TCP_PORT 55555
#define MAX_CLIENTS 50
#define TCP_INIT_PACKET_SIZE (sizeof(u16_t) + 2 * MAC_SIZE)

#define TCP_WRITE(pcb, buffer, size, flags) 			\
	do {												\
		tcp_write((pcb), (buffer), (size), (flags));	\
		tcp_output((pcb));								\
	} while(0)											\

/* Protocol messages and constants */
#define ACCEPT "ACCEPT"
#define REJECT "REJECT"
#define REJECT_NO_SLOTS (REJECT " - No slots")
#define REJECT_WRONG_AUTH (REJECT " - Wrong authentication message")

#define TCP_AUTHENTICATION_MESSAGE(buffer, packetSize) (	\
	PROT_AUTH_MESSAGE0(buffer, packetSize) )

#define PROT_AUTH_MESSAGE0(buffer, packetSize) (	\
	(packetSize) == 13 && (buffer)[12] == 0x00 )

#endif

